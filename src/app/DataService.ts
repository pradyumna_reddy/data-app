import { Injectable } from "@angular/core";






@Injectable()
export class DataService{
    dataChunk = [{
        "id": "1CbNvkcibk97kcBuoLy7YahR6AcNZDe1uq",
        "first_name": "Brandea",
        "pic": "http://dummyimage.com/192x223.bmp/ff4444/ffffff"
      }, {
        "id": "19Hou8HiK4zNTkFSKX9RApcoBRGJMSQQkz",
        "first_name": "Archibald",
        "pic": "http://dummyimage.com/121x127.bmp/ff4444/ffffff"
      }, {
        "id": "1GHLipjMqxzqu1Z28NfHTimYMvqbqEWvC3",
        "first_name": "Alexis",
        "pic": "http://dummyimage.com/200x170.jpg/5fa2dd/ffffff"
      }, {
        "id": "1L2yVyVgbPsLEV3Gbvu1kpqRzVt3tjNMKM",
        "first_name": "Thorstein",
        "pic": "http://dummyimage.com/215x130.bmp/dddddd/000000"
      }, {
        "id": "1MRfpxCdde8JUCrNV7DkNLrauM2F3PsjQw",
        "first_name": "Otis",
        "pic": "http://dummyimage.com/230x153.jpg/dddddd/000000"
      }, {
        "id": "1JFveo7mV7ypQakTmp1aPsrX2bfWiD7Cg4",
        "first_name": "Tommy",
        "pic": "http://dummyimage.com/129x186.jpg/cc0000/ffffff"
      }, {
        "id": "1Gjna38ZYMS9DBqbau68tdFaBBHidGQ8sM",
        "first_name": "Melessa",
        "pic": "http://dummyimage.com/223x206.jpg/cc0000/ffffff"
      }, {
        "id": "19gQFBDDxkeDUPmuBeRdtnJ3VAVeFFnQb1",
        "first_name": "Lewie",
        "pic": "http://dummyimage.com/170x235.png/cc0000/ffffff"
      }, {
        "id": "1JtAbJBJ27bfVV8HxcBwQGT38ULGuU8Kv7",
        "first_name": "Jessey",
        "pic": "http://dummyimage.com/240x106.bmp/cc0000/ffffff"
      }, {
        "id": "165esYyXTRiXhHxvyQhvCCu7M9j2pn8NAU",
        "first_name": "Meriel",
        "pic": "http://dummyimage.com/159x131.bmp/ff4444/ffffff"
      }];
    

      getData(){
          return this.dataChunk;
      }
}