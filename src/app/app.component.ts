import { Component } from '@angular/core';
import {DataService} from './DataService';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  ext = 'MetaApp';


  userDetails;

  catchData(x){
    console.log(x);


    this.ext = x;
  }


  constructor(private dataSer: DataService){
    this.userDetails = this.dataSer.getData();
  }
}
