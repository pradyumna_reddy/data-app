import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input()
  data;

  @Output()
  sendData = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  sendDataToParent(){
    this.sendData.emit(this.data.first_name)
  }

}
